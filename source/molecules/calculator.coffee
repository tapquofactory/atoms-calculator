"use strict"

class Atoms.Molecule.Calculator extends Atoms.Molecule.Div

  @extends  : true

  @events   : ["submit"]

  @default  :
    children: [
      "Atom.Button": id: "c", name: "c", text: "c", style: "big padding right cancel"
    ,
      "Atom.Input": id: "text", name: "text", style: "fluid small right", disabled:true
    ,
      "Atom.Button": id: "del", name: "del", icon: "left-arrow", style: "big right warning"
    ,
      "Atom.Input": id: "input", name: "input", autofocus:"autofocus", type: "text", placeholder: "Input", events: ["keyup"], style: "fluid big left"
    ,
      "Molecule.Div": id: "numbers", name: "numbers", style: "left"
    ,
      "Atom.Button": text: "+", style: "big padding right"
    ,
      "Atom.Button": text: "*", style: "big padding right"
    ,
      "Atom.Button": text: "-", style: "big padding right"
    ,
      "Atom.Button": text: "/", style: "big padding right"
    ]

  @param1 : 0
  @operation: ""

  constructor: ->
    super
    #Dinamically load number buttons:
    for _i in [9..0] by -1
      @numbers.appendChild "Atom.Button", {text: "#{_i}", style: "big padding right"}
    #= into numbers div"
    @numbers.appendChild "Atom.Button", {text: "=", style: "big padding right accept"}
    @del.el.hide()


  # -- Children Bubble Events --------------------------------------------------
  onInputKeyup: (event, atom) ->
    do @showDel
    if event.keyCode is 111 # /
      @saveCurrent "/"
    else if event.keyCode is 106 # *
      @saveCurrent "*"
    else if event.keyCode is 109 # -
      @saveCurrent "-"
    else if event.keyCode is 107 # +
      @saveCurrent "+"
    else if event.keyCode is 13 # enter
      @operate @input.value()

  onButtonTouch: (event, atom) ->
    if atom.attributes.text in ["+","-","*","/"] and @input.value() not in ["+","-","*","/"] and not isNaN @input.value()
      @saveCurrent atom.attributes.text
    else
      if atom.attributes.text is "="
        @operate @input.value()
      else if atom.attributes.name is "del"
        @input.value @input.value().slice(0, -1)
        do @showDel
      else if atom.attributes.name is "c"
        do @onC
      else
        @input.value @input.value()+atom.attributes.text
        do @showDel

  saveCurrent:(value)->
    @input.value @input.value().slice(0, -1) if @input.value().indexOf(value)>0
    @param1 = @input.value()
    @operation = value
    @input.value ""
    do @showDel

  operate:(param2)->
    if @operation
      @text.value "#{@param1}#{@operation}#{param2}"
      @input.value eval "#{@param1}#{@operation}#{param2}"
      @param1 = @input.value()
      @operation = ""

  showDel:()->
    if @input.value() then @del.el.show() else @del.el.hide()

  onC:()->
    @text.value ""
    @input.value ""
    @param1 = 0
    @operation = ""
    @del.el.hide()
